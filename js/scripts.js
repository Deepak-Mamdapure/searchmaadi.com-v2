$(document).ready(function () {

//+++++++++++++++++++++++++++++++++++++++++++
//     Jquery Code Start
//+++++++++++++++++++++++++++++++++++++++++++     
    $(function () {

        $(".input-group-btn .dropdown-menu li a").click(function () {

            var selText = $(this).html();
            $(this).parents('.input-group-btn').find('.btn-search').html(selText);

        });

    });

//    Brand Slider

    $('#brandSlider').owlCarousel({
        loop: true,
        margin: 80,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                dots:false
            },
            600: {
                items: 3,
                nav: false,
                margin: 20
            },
            1000: {
                items: 4
            }
        }
    });

    $('#trend-cat').owlCarousel({
        loop: true,
        margin: 25,
        dots:false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false,
                margin: 20
            },
            991: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    });


   $('#category-box').owlCarousel({
        loop: true,
        margin: 25,
        dots:false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false,
                margin: 20
            },
            1000: {
                items: 4
            }
        }
    });

//    City Slider

    $("#citySlider").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items: 1,
        autoplay: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: true,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                nav: false
            },
            768: {
                nav: true
            }
        }
    });

//    open Slide Function
    $(".namma-slides").addClass("openSlides");
    $(".social-sidebar").addClass("marginTop");
    $(".namma-box").addClass("openSlide");
    $(".namma-box ul li a").click(function(e){
        e.preventDefault();
        $(this).parent("li").parent("ul").parent(".namma-box").addClass("openSlide");
        $(this).parent("li").parent("ul").parent(".namma-box").next(".namma-slides").addClass("openSlides");
        $(".social-sidebar").addClass("marginTop");
    });

    $(".namma-slides h3").click(function(){
        $(".namma-box").removeClass("openSlide");
        $(".namma-slides").removeClass("openSlides");
        $(".social-sidebar").removeClass("marginTop");
    });

//    Event Box

    $("#eventBox ul li a").click(function(e){
        e.preventDefault();
        $(".sidebar-main .sidebar-box").addClass("eventShow");
        $(this).parent("li").parent("ul").parent("#eventBox").addClass("closeEvent");
        $("#justBox").addClass("closeEvent");
        $("#offerBox").addClass("closeEvent");
    });
    $(".event-box h3").click(function(e){
        e.preventDefault();
        $(".sidebar-main .sidebar-box").removeClass("eventShow");
        $("#eventBox").removeClass("closeEvent");
        $("#justBox").removeClass("closeEvent");
        $("#offerBox").removeClass("closeEvent");
    });

    $("#justBox ul li a").click(function(e){
        e.preventDefault();
        $(".sidebar-main .user-pro").addClass("eventShow");
        $(this).parent("li").parent("ul").parent("#justBox").addClass("closeEvent");
        $("#justBox").addClass("closeEvent");
        $("#offerBox").addClass("closeEvent");
        $("#eventBox").addClass("closeEvent");
        $(".justTab").addClass("active").removeClass("closeTab");
        $("#home").addClass("active").addClass("in");
        $(".offerTab").addClass("closeTab");
    });

    $(".user-pro .nav-tabs li a[href='#home']").click(function(e){
        e.preventDefault();
        $(".sidebar-main .user-pro").removeClass("eventShow");
        $(this).parent("li").parent("ul").parent("#justBox").removeClass("closeEvent");
        $("#justBox").removeClass("closeEvent");
        $("#offerBox").removeClass("closeEvent");
        $("#eventBox").removeClass("closeEvent");
    });

    $("#offerBox ul li a").click(function(e){
        e.preventDefault();
        $(".sidebar-main .user-pro").addClass("eventShow");
        $(this).parent("li").parent("ul").parent("#offerBox").addClass("closeEvent");
        $("#justBox").addClass("closeEvent");
        $("#offerBox").addClass("closeEvent");
        $("#eventBox").addClass("closeEvent");
        $(".justTab").addClass("closeTab");
        $(".offerTab").addClass("active").removeClass("closeTab");
        $("#menu1").addClass("active").addClass("in");
    });

    $(".user-pro .nav-tabs li a[href='#menu1']").click(function(e){
        e.preventDefault();
        $(".sidebar-main .user-pro").removeClass("eventShow");
        $(this).parent("li").parent("ul").parent("#offerBox").removeClass("closeEvent");
        $("#justBox").removeClass("closeEvent");
        $("#offerBox").removeClass("closeEvent");
        $("#eventBox").removeClass("closeEvent");
    });

    $("#showVisitors").click(function(e){
        e.preventDefault();
        $(this).parent("li").parent("ul").parent(".visitor-box").toggleClass("showVisits");

    });


//Owl Carousel

    $("#eventSlider").owlCarousel({
        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items: 1,
        autoplay: true,
        dots: true,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    });

//
    $("#eventSliders").owlCarousel({
        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items: 1,
        autoplay: true,
        dots: true,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    });
    $('.selectpicker').selectpicker();

        $("#news-container").vTicker({
            speed: 500,
            pause: 3000,
            animation: 'fade',
            mousePause: false,
            showItems: 11
        });
    $("#news-container1").vTicker({
            speed: 1000,
            pause: 4000,
            animation: 'fade',
            mousePause: false,
            showItems:11
        });

    //.................

    $("[data-toggle=popover]").each(function(i, obj) {

        $(this).popover({
            html: true,
            content: function() {
                var id = $(this).attr('id')
                return $('#popover-content-' + id).html();
            }
        });

    });
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });

//    Map Popover remove

    $(document).on("click", ".popover .crossIcon" , function(e){
        e.preventDefault();
        $(this).parents(".popover").popover('hide');
    });

//    Navbar Fixed

    var lastScroll = 10;
    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScroll){
            $(".main-menu .navbar-default").addClass("bg_change");
        } else {
            $(".main-menu .navbar-default").removeClass("bg_change");
        }
    });
});

// Navbar Fixed for Other Pages Except Home Page

$(window).scroll(function() {    
var scroll = $(window).scrollTop();
 //alert(scroll);
if (scroll >= 50) {
    //console.log('a');
    $("#banner-wrap, .main-menu .navbar-default").addClass("bg_change");
} else {
    //console.log('a');
    $("#banner-wrap, .main-menu .navbar-default").removeClass("bg_change");
}
});

// Auto Popup
$(document).ready(function(){
	$(".PageLoadPop").modal('show');
});

// Ongoing / Upcoming Events

$(document).ready(function(){
    $(".UpComingBtn").click(function(){
        $(".OnGoingBtn").removeClass("active");
        $(".UpComingBtn").addClass("active");
		$("#OnGoingEvent").removeClass("active");
        $("#UpComingEvent").addClass("active");
    });
    $(".OnGoingBtn").click(function(){
        $(".UpComingBtn").removeClass("active");
        $(".OnGoingBtn").addClass("active");
		$("#UpComingEvent").removeClass("active");
        $("#OnGoingEvent").addClass("active");
    });
});